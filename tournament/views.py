# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import logout,login
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpRequest, response,SimpleCookie,cookie
from django.shortcuts import render,render_to_response, get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View
from django.urls import reverse
from django.contrib.auth.models import User,Group
from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import render
from types import MethodType
from utils_tournament import *
from  utils_articles import *

# def get(self, request):
from django.contrib import auth
from django.views.generic.list import ListView

from tournament.forms import new_sportman
from django.contrib import messages
from tournament.models import *


class Home(View):
        def  get (self,request):
                all_articles=sort_reverse_articles(list(Articles.objects.all()))
                context={
                    'all_articles':all_articles,
                    'user':request.user,
                }
                return  render(request,'tournament/articles_all.html',context)

def one_article(request,*args,**kwargs):
    buf_one_article=Articles.objects.get(id=kwargs.get('article_id'))
    context={
        'one_article':buf_one_article
    }
    return render(request,'tournament/articles_one.html',context)

class Authentication(View):
        def get(self,request):
                return render(request,'tournament/authentication.html')

class Check_in(View):
        def get(self,request):
                return render(request,'tournament/check_in.html')
        def post(self,request):
            if len(request.POST.get('username')) ==0 or len(request.POST.get('password'))==0 or len(request.POST.get('email'))==0 or len(request.POST.get('firstName'))==0 or len(request.POST.get('lastName'))==0:
                messages.add_message(request, messages.ERROR, 'Необходимо заполнить все поля')
            else:
                if User.objects.filter(username=request.POST.get('username')).exists():
                    messages.add_message(request, messages.ERROR, 'Такой пользователь уже существует')
                    context = {
                        'message': messages
                    }
                    return HttpResponseRedirect(reverse('check_in'), context)
                else:
                    new_user = User.objects.create_user(username=request.POST.get('username'),
                                                        password=request.POST.get('password'),
                                                        email=request.POST.get('email'))
                    new_user.first_name = request.POST.get('firstName')
                    new_user.last_name = request.POST.get('lastName')
                    group = Group.objects.get(name='Guests')
                    new_user.groups.add(group)
                    new_user.is_staff = False
                    new_user.is_active = True
                    new_user.is_superuser = False
                    Group.objects.get(name='Guests').user_set.add(new_user)
                    new_user.save()
                    login(request, new_user)
                    return HttpResponseRedirect(reverse('home'))

            context = {
                'message': messages
            }
            return HttpResponseRedirect(reverse('check_in'), context)


class UserAuthenticate(View):
        def get(self,request,*args,**kwargs):
                if request.user.has_perm('tournament.add_tournaments') and request.user.is_superuser!=True:
                    if Tournaments.objects.filter(tournamentsid=kwargs.get('tournament_id'),finishtournament=False).exists():
                        tournament=Tournaments.objects.get(tournamentsid=kwargs.get('tournament_id'),finishtournament=False)
                        tournament.delete()
                    buf_user = User.objects.get(id=request.user.id)
                    buf_user.userprofile.start_tournament = False
                    buf_user.save()
                    buf_user.userprofile.save()
                logout(request)
                return HttpResponseRedirect(reverse('home'))
        def post(self,request):
                user=auth.authenticate(username=request.POST.get('username'),
                                       password=request.POST.get('password'))
                if user is not None and user.is_active:
                        login(request,user)
                        return HttpResponseRedirect(reverse('home'))
                else:
                    messages.add_message(request, messages.ERROR,'Пользователя с таким логином или паролем не существует ')

                    context = {
                        'message': messages
                    }
                    return  render(request,'tournament/authentication.html',context)

class AboutTournament(View):
    def get(self,request):
        return render(request,'tournament/about_tournament.html')

class AthleteCard(View):
    def get(self,request, *args, **kwargs):
        if request.user.has_perm('tournament.add_tournaments'):
            urls = request.path_info
            Sportman_id=kwargs.get('Sportman_id')

            all_sportman=Sportman.objects.all()
            for a in all_sportman:
                print(a.genderman)
            string_search=request.GET.get('search_string')
            if(string_search!=None):
                if(request.GET.get('search_')=='1'):#FIO
                    buf_all_sportman=Sportman.objects.filter(sportmanname__startswith=string_search)
                else:
                    buf_all_sportman = Sportman.objects.filter(numberpassport__startswith=string_search)
                if(buf_all_sportman.count()!=0):
                    all_sportman=buf_all_sportman
                else:
                    messages.add_message(request, messages.ERROR, 'Карта спортсмена не найдена')


            if (urls[:20]== '/athlete_card/delete'):
                sportman_delete = get_object_or_404(Sportman, sportmanid=Sportman_id)
                print (sportman_delete.genderman)
                sportman_delete.delete()
                all_sportman = Sportman.objects.all()
                return HttpResponseRedirect("/athlete_card/")

            context = {
                'all_sportman': all_sportman,
                'message': messages

            }
            return render(request, 'tournament/athlete_card.html', context)
        else:
            return HttpResponseRedirect("/accounts/login/")


    def post(self, request, *args, **kwargs):
      if request.user.has_perm('tournament.add_tournaments'):
        urls=request.path_info
        print (urls)
        Sportman_id = kwargs.get('Sportman_id')
        if(urls == '/athlete_card/add/'):
            if Sportman.objects.filter(numberpassport=request.POST.get('number_passport')).exists():
                messages.add_message(request, messages.ERROR, 'Спортсмен c номером паспорта ['+request.POST.get('number_passport') +'] уже зарегистрирован')
            else:
                gender=False
                if request.POST.get('gender_man') == '1':
                    gender=True
                new_sportman = Sportman.objects.create(sportmanname=request.POST.get('sportman_name'),
                                               ratingelo=request.POST.get('rating_elo'),
                                               birthday=request.POST.get('birthday'),
                                               countgame=request.POST.get('count_game'),
                                               numberpassport=request.POST.get('number_passport'),
                                               genderman=gender)
                new_sportman.save()

        if(urls == '/athlete_card/edit/'):
            if Sportman.objects.filter(numberpassport=request.POST.get('number_passport')).exists():
                valid_sportman = Sportman.objects.get(numberpassport=request.POST.get('number_passport'))
                if valid_sportman.sportmanid!=int(request.COOKIES.get('cookie_sportman_id')):
                    messages.add_message(request, messages.ERROR, 'Спортсмен c номером паспорта ['+request.POST.get('number_passport') +'] уже зарегистрирован')
                else:
                    gender = False
                    if request.POST.get('gender_man') == '1':
                        gender = True
                    print ('NAME')
                    print request.POST.get('sportman_name')
                    Sportman_id = request.COOKIES.get('cookie_sportman_id')
                    sportman_edit = get_object_or_404(Sportman, sportmanid=Sportman_id)
                    sportman_edit.sportmanname = request.POST.get('sportman_name')
                    buf_rating_elo = float(request.POST.get('rating_elo'))
                    sportman_edit.ratingelo = buf_rating_elo
                    sportman_edit.birthday = request.POST.get('birthday')
                    buf_count_game = int(request.POST.get('count_game'))
                    sportman_edit.countgame = buf_count_game
                    sportman_edit.numberpassport = request.POST.get('number_passport')
                    sportman_edit.genderman = gender
                    sportman_edit.save()
            else:
                gender = False
                if request.POST.get('gender_man') == '1':
                    gender = True
                print ('NAME')
                print request.POST.get('sportman_name')
                Sportman_id=request.COOKIES.get('cookie_sportman_id')
                sportman_edit = get_object_or_404(Sportman, sportmanid=Sportman_id)
                sportman_edit.sportmanname=request.POST.get('sportman_name')
                buf_rating_elo=float(request.POST.get('rating_elo'))
                sportman_edit.ratingelo = buf_rating_elo
                sportman_edit.birthday = request.POST.get('birthday')
                buf_count_game=int(request.POST.get('count_game'))
                sportman_edit.countgame = buf_count_game
                sportman_edit.numberpassport = request.POST.get('number_passport')
                sportman_edit.genderman = gender
                sportman_edit.save()

        context = {
            'message': messages

        }
        return HttpResponseRedirect("/athlete_card/",context)

      else:
            return HttpResponseRedirect("/accounts/login/")

@permission_required('tournament.add_tournaments')
def  tournament_new(request):
    if(request.POST):
        gender = False
        if request.POST.get('gender_man') == '1':
            gender = True
        all_sportman_gender=Sportman.objects.filter(genderman=gender)
        if (all_sportman_gender.count()<3):
            messages.add_message(request,messages.ERROR,'Для продолжения необходимо зарегистрировать больше двух спортсменов')
            context = {
                'message': messages
            }
            return render(request, 'tournament/tournament_new.html',context)
        else:
            tournament_new = Tournaments.objects.create(judgename=request.POST.get('judge_name'),
                                                        location=request.POST.get('location'),
                                                        datetournament=datetime.now(),
                                                        finishtournament=False,
                                                        nametournament=request.POST.get('tournament_name'),
                                                        genderman=gender,
                                                        counttours=0
                                                        )
            tournament_new.save()
            return_respons_rederect=HttpResponseRedirect(reverse('tournament_new_list_participant'))
            return_respons_rederect.set_cookie('tournament_id',tournament_new.tournamentsid)
            return_respons_rederect.set_cookie('tours_number', 1)
            return return_respons_rederect

    return render(request,'tournament/tournament_new.html')

@permission_required('tournament.add_tournaments')
def  tournament_new_list_participant(request,*args,**kwargs):
    tournament_new=get_object_or_404(Tournaments, tournamentsid=request.COOKIES.get('tournament_id'))
    gender_tournament = tournament_new.genderman

    all_sportman=list( Sportman.objects.filter(genderman=gender_tournament))
    all_spotman_object=Sportman.objects.filter(genderman=gender_tournament)
    if request.POST:
        string_search = request.POST.get('search_string')
        if (string_search != None):
            if (request.POST.get('search_') == '1'):  # FIO
                buf_all_sportman = Sportman.objects.filter(genderman=gender_tournament,sportmanname__startswith=string_search)
            else:
                buf_all_sportman = Sportman.objects.filter(genderman=gender_tournament,numberpassport__startswith=string_search)
            if (buf_all_sportman.count() != 0):
                all_spotman_object = buf_all_sportman
            else:
                messages.add_message(request, messages.ERROR, 'Спортсменов не найдено')

    all_participants = Particianttournaments.objects.filter(
        tournaments_tournamentsid=request.COOKIES.get('tournament_id'))

    all_sportman=list(all_spotman_object)
    for sportman in all_spotman_object:
        buf = Particianttournaments.objects.filter( sportman_sportmanid=sportman,tournaments_tournamentsid=tournament_new)
        if  buf.exists():
            all_sportman.remove(sportman)
    if(request.POST.get('btn_start')=='start'):
        partipants=Particianttournaments.objects.filter(tournaments_tournamentsid=tournament_new)
        if partipants.count()>=3:
            tournament_new.add_count_tours()
            user=User.objects.get(id=request.user.id)
            user.start_tournament=True
            user.save()
            return HttpResponseRedirect(reverse('tournament_new_tour',kwargs={'Tour_number':1 }))
        else:
            messages.add_message(request,messages.ERROR,'Для начала турнира необходимо выбрать не менее трех спортсменов')

    context={
        'all_sportman':all_sportman,
        'all_participants':all_participants,
        'message':messages,
        'tournament':tournament_new,
        'activ_tournament':False,
    }
    return render(request,'tournament/tournament_new_lisl_participant.html',context)

@permission_required('tournament.add_tournaments')
def tournamen_new_add_participant(request,*args,**kwargs):
    tournament_new = get_object_or_404(Tournaments, tournamentsid=request.COOKIES.get('tournament_id'))
    sportman_to_participant=get_object_or_404(Sportman,sportmanid=kwargs.get('Sportman_id'))
    if Particianttournaments.objects.filter(sportman_sportmanid=kwargs.get('Sportman_id'),tournaments_tournamentsid=request.COOKIES.get('tournament_id')).exists():
        return HttpResponseRedirect(reverse('tournament_new_list_participant'))
    else:
        new_participant=Particianttournaments.objects.create(
        tournaments_tournamentsid=tournament_new,
        sportman_sportmanid=sportman_to_participant,
        currentpoint=0,
        abscolorchess=0,
        nexttour=False)
        new_participant.save()
    return HttpResponseRedirect(reverse('tournament_new_list_participant'))

@permission_required('tournament.add_tournaments')
def tournamen_new_delete_participant(request,*args,**kwargs):
    if Particianttournaments.objects.filter(particianttournamentid=kwargs.get('Participant_id'),tournaments_tournamentsid=request.COOKIES.get('tournament_id')).exists():
        participant_to_sportman = get_object_or_404(Particianttournaments,
                                                    particianttournamentid=kwargs.get('Participant_id'))
        participant_to_sportman.delete()
    return HttpResponseRedirect(reverse('tournament_new_list_participant'))


@permission_required('tournament.add_tours')
def tournament_new_tour(request,*arg,**kwargs):
    buf_user = User.objects.get(id=request.user.id)
    if buf_user.userprofile is None:
        buf_user_profil = UserProfile.objects.create(start_tournament=True, user=buf_user)
        buf_user.userprofile = buf_user_profil
        buf_user.save()
        buf_user_profil.save()
    else:
        buf_user.userprofile.start_tournament = True
        buf_user.save()
        buf_user.userprofile.save()
    urls = request.path_info
    buf_number_tour=kwargs.get('Tour_number')
    tournament_new = get_object_or_404(Tournaments, tournamentsid=request.COOKIES.get('tournament_id'))


    all_participant_tours = list(Participanttours.objects.filter(tours_tourid__numbertour=buf_number_tour,
                                particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament_new.tournamentsid))

    if int(buf_number_tour) <= int(tournament_new.counttours) and len(all_participant_tours)==0:
            new_tour=Tours()
            new_tour.numbertour=buf_number_tour
            if int(buf_number_tour) == 1:
                pair_for_firs_tour(tournament_new)
            else:
                pair_for_next_tour(tournament_new,buf_number_tour)
    all_participant_tours = list(Participanttours.objects.filter(tours_tourid__numbertour=buf_number_tour,
                                                                 particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament_new.tournamentsid))
    participant_next_tour = list(Participanttours.objects.filter(tours_tourid__numbertour=buf_number_tour,
                                                                 particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament_new.tournamentsid,
                                                                 groupnumber=0))

    all_couple = list()
    count = 0

    while count < int(len(all_participant_tours) / 2):
        print len(all_participant_tours)
        participants_one_couple = Participanttours.objects.filter(tours_tourid__numbertour=buf_number_tour,
                                                                  particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament_new.tournamentsid,
                                                                  groupnumber=count + 1)
        print len(participants_one_couple)
        buf_couple = couple_participant_tour(participants_one_couple[0], participants_one_couple[1])
        count += 1
        all_couple.append(buf_couple)
    if (request.POST.get('search_string')):
        string_search = request.POST.get('search_string')
        if (string_search != None):
               buf_all_couple = Participanttours.objects.filter(tours_tourid__numbertour=buf_number_tour,
                                particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament_new.tournamentsid,
                                groupnumber=string_search)
               if len(buf_all_couple)!=0:
                    all_couple=[]
                    all_couple.append(couple_participant_tour(buf_all_couple[0], buf_all_couple[1]))
               else:
                    messages.add_message(request, messages.ERROR, 'Игроков не найдено')

    if request.POST.get('couple_one_F.I.O.'):
        participant_one_id=request.COOKIES.get('cookie_couple_id_one')
        participant_couple_one=Participanttours.objects.get(participanttoursid=participant_one_id)
        participant_couple=list(Participanttours.objects.filter(tours_tourid=participant_couple_one.tours_tourid,groupnumber=participant_couple_one.groupnumber))
        participant_couple.remove(participant_couple_one)
        participant_couple_two=Participanttours.objects.get(participanttoursid=participant_couple[0].participanttoursid)
        point_tour_one=request.POST.get('Point_tour_one')
        if participant_couple_one.finishgroup is True:
            participant_couple_one.particianttournaments_particianttournamentid.currentpoint-=participant_couple_one.pointtour
            participant_couple_one.particianttournaments_particianttournamentid.save()
            participant_couple_two.particianttournaments_particianttournamentid.currentpoint -= participant_couple_two.pointtour
            participant_couple_two.particianttournaments_particianttournamentid.save()

            participant_couple_one.particianttournaments_particianttournamentid.sportman_sportmanid.ratingelo-=participant_couple_one.elotour
            participant_couple_one.particianttournaments_particianttournamentid.sportman_sportmanid.save()
            participant_couple_two.particianttournaments_particianttournamentid.sportman_sportmanid.ratingelo-=participant_couple_two.elotour
            participant_couple_two.particianttournaments_particianttournamentid.sportman_sportmanid.save()

        participant_couple_one.pointtour = point_tour_one
        participant_couple_one.finishgroup=True

        participant_couple_two.pointtour = 1 - float(participant_couple_one.pointtour)
        participant_couple_two.finishgroup=True

        couple_add_ratingelo(participant_couple_one,participant_couple_two)

        participant_couple_one.save()
        participant_couple_two.save()
        return HttpResponseRedirect(urls)

    if (request.POST.get('btn_sfinish_tour') == 'finish_tour'):
        if(int(buf_number_tour) < int(tournament_new.counttours)):
            if len(Participanttours.objects.filter(finishgroup=False,tours_tourid__numbertour=buf_number_tour,particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament_new.tournamentsid))> 0:
                messages.add_message(request, messages.ERROR, 'Тур сыгран не всеми парами')
            else:
                buf_number_tour=int(buf_number_tour)
                buf_number_tour+=1
                return HttpResponseRedirect(reverse('tournament_new_tour', kwargs={'Tour_number': buf_number_tour}))
        else:
            tournament_new.finishtournament=True
            tournament_new.save()
            distribution_of_prizes(tournament_new)

            buf_user = User.objects.get(id=request.user.id)
            buf_user.userprofile.start_tournament = False
            buf_user.save()
            buf_user.userprofile.save()
            return_respons_rederect = HttpResponseRedirect(reverse('tournament_new_list_participant'))
            return_respons_rederect.set_cookie('tournament_id', tournament_new.tournamentsid)


            return HttpResponseRedirect(reverse('tournament_new_tour_result',kwargs={'Tournament_id':tournament_new.tournamentsid }))

    context={
        'participant_next': participant_next_tour,
        'tournament':tournament_new,
        'current_tour':buf_number_tour,
        'all_couple':all_couple,
        'message': messages,
    }
    return_respons_rederect = render(request, 'tournament/tournament_new_tour.html',context)
    return_respons_rederect.set_cookie('tours_number', buf_number_tour)
    return return_respons_rederect

@login_required
def tournament_result(request,*args,**kwargs):
    tournament = get_object_or_404(Tournaments, tournamentsid=kwargs.get('Tournament_id'))
    all_participant_tournament=list()
    if request.POST.get('search_string'):
        buf_all_participant_tournament = list(Particianttournaments.objects.filter(tournaments_tournamentsid=tournament,sportman_sportmanid__sportmanname__startswith=request.POST.get('search_string')))
        if len(buf_all_participant_tournament)>0:
            all_participant_tournament=buf_all_participant_tournament
        else:
            messages.add_message(request, messages.ERROR, 'По вашему запросу ничего не найдено')
            all_participant_tournament = list(
            Particianttournaments.objects.filter(tournaments_tournamentsid=tournament))

    else:
        all_participant_tournament = list(Particianttournaments.objects.filter(tournaments_tournamentsid=tournament))

    all_participant_tournament.sort(key=order_by_place, reverse=True)
    context = {
        'all_participant_tournament': all_participant_tournament,
        'tournament': tournament,
        'message': messages,
    }
    return render(request,'tournament/tournamen_new_result_table.html',context)


@login_required()
def archive_tournsmenst(request):
    all_tournament=list()
    if request.POST.get('search_string'):
        gender=False
        if request.POST.get('search_')=='1':
            gender=True
        buf_tournament=Tournaments.objects.filter(finishtournament=True,nametournament__startswith=request.POST.get('search_string'),genderman=gender)
        if (len(buf_tournament))>0:
            all_tournament=buf_tournament
        else:
            messages.add_message(request, messages.ERROR, 'По вашему запросу ничего не найдено')
            all_tournament = Tournaments.objects.filter(finishtournament=True)
    else:
        all_tournament = Tournaments.objects.filter(finishtournament=True)
    context={
        'all_tournaments':all_tournament,
        'message': messages,
    }
    return render(request,'tournament/tournament_archive.html',context)

def delete_tournament(request,*args,**kwargs):
    tournament_new = get_object_or_404(Tournaments, tournamentsid=kwargs.get('tournament_id'))
    tournament_new.delete()
    buf_user = User.objects.get(id=request.user.id)
    buf_user.userprofile.start_tournament = False
    buf_user.save()
    buf_user.userprofile.save()
    return HttpResponseRedirect(reverse('home'))