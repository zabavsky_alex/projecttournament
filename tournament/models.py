# -*- coding: utf-8 -*-
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.contrib.auth.models import User
from datetime import datetime
from django.db import models
import math

class Test(models.Model):
    image=models.ImageField(null=True,blank=True,upload_to="images/",verbose_name='Изображение')

class Articles(models.Model):
    name=models.CharField(max_length=100,verbose_name='Название статьи')
    text=models.TextField(verbose_name='Текст')
    publication_date=models.DateTimeField(verbose_name='Дата публикации')
    image = models.ImageField(null=True, blank=True, upload_to="images/", verbose_name='Изображение')

    def location(self):
        if self.id%2==0:
            return False
        else:
            return True

    def short_text(self):
        if len(self.text)>100:
            buf_text=self.text[:110]
            buf_text+='....'
            return buf_text
        else:
            return self.text

class MySQLBooleanField(models.BooleanField):

    def to_python(self, value):
        if isinstance(value, bool):
            return value
        return bytearray(value)[0]

    def from_db_value(self, value, expression, connection, context):
        if value=='\x01' or value == True:
            return True
        else:
            return False

class Particianttournaments(models.Model):
    particianttournamentid = models.AutoField(db_column='ParticiantTournamentId', primary_key=True)  # Field name made lowercase.
    place = models.IntegerField(db_column='Place', blank=True, null=True)  # Field name made lowercase.
    buchholzcoefficient = models.FloatField(db_column='BuchholzCoefficient', blank=True, null=True)  # Field name made lowercase.
    solkoffcoefficient = models.FloatField(db_column='SolkoffCoefficient', blank=True, null=True)  # Field name made lowercase.
    solkoffcoefficienttruncated = models.FloatField(db_column='SolkoffCoefficientTruncated')  # Field name made lowercase.
    currentpoint = models.FloatField(db_column='CurrentPoint')  # Field name made lowercase.
    abscolorchess = models.IntegerField(db_column='AbsColorChess', blank=True, null=True)  # Field name made lowercase.
    truncatedsolkoffcoefficient = models.FloatField(db_column='TruncatedSolkoffCoefficient', blank=True, null=True)  # Field name made lowercase.
    sportman_sportmanid = models.ForeignKey('Sportman', models.DO_NOTHING, db_column='Sportman_SportmanId')  # Field name made lowercase.
    tournaments_tournamentsid = models.ForeignKey('Tournaments', models.DO_NOTHING, db_column='Tournaments_TournamentsId')  # Field name made lowercase.
    nexttour = MySQLBooleanField(db_column='NextTour')  # Field name made lowercase.



    class Meta:
        managed = False
        db_table = 'ParticiantTournaments'



class Participanttours(models.Model):
    participanttoursid = models.AutoField(db_column='ParticipantToursId', primary_key=True)  # Field name made lowercase.
    colorchess = MySQLBooleanField(db_column='ColorChess')  # Field name made lowercase. This field type is a guess.
    elotour = models.IntegerField(db_column='Elo_tour')  # Field name made lowercase.
    groupnumber = models.IntegerField(db_column='GroupNumber')  # Field name made lowercase.
    pointtour = models.FloatField(db_column='PointTour', blank=True, null=True)  # Field name made lowercase.
    finishgroup = MySQLBooleanField(db_column='FinishGroup')  # Field name made lowercase. This field type is a guess.
    particianttournaments_particianttournamentid = models.ForeignKey(Particianttournaments, models.DO_NOTHING, db_column='ParticiantTournaments_ParticiantTournamentId')  # Field name made lowercase.
    tours_tourid = models.ForeignKey('Tours', models.DO_NOTHING, db_column='Tours_TourId')  # Field name made lowercase.



    class Meta:
        managed = False
        db_table = 'ParticipantTours'


class Ranksportman(models.Model):
    ranksportmanid = models.AutoField(db_column='RankSportmanId', primary_key=True)  # Field name made lowercase.
    rankname = models.CharField(db_column='RankName', max_length=70)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RankSportman'


class Sportman(models.Model):
    sportmanid = models.AutoField(db_column='SportmanId', primary_key=True)  # Field name made lowercase.
    birthday = models.DateTimeField(db_column='BirthDay')  # Field name made lowercase.
    ratingelo = models.IntegerField(db_column='RatingElo')  # Field name made lowercase.
    sportmanname = models.CharField(db_column='SportmanName', max_length=50) # Field name made lowercase.
    countgame = models.IntegerField(db_column='CountGame')  # Field name made lowercase.
    genderman = MySQLBooleanField(db_column='GenderMan')  # Field name made lowercase. This field type is a guess.
    currentmatchcoefficient = models.IntegerField(db_column='CurrentMatchCoefficient', blank=True, null=True)  # Field name made lowercase.
    numberpassport = models.CharField(db_column='NumberPassport', max_length=30)  # Field name made lowercase.
    ranksportman_ranksportmanid = models.ForeignKey(Ranksportman, models.DO_NOTHING, db_column='RankSportman_RankSportmanId')  # Field name made lowercase.

    def age (self):
        today = datetime.today()
        age = today.year - self.birthday.year
        if today.month < self.birthday.month:
            age -= 1
        elif today.month == self.birthday.month and today.day < self.birthday.day:
            age -= 1
        return age

    def age_date_month(self):
        buf_month=self.birthday.month
        if buf_month<10:
            ret='0'
            ret+=str(buf_month)
            return (ret)
        else:
            return buf_month

    def age_date_year(self):
        return self.birthday.year

    def age_date_day(self):
        buf_day = self.birthday.month
        if buf_day < 10:
            ret = '0'
            ret += str(buf_day)
            return (ret)
        else:
            return buf_day

    class Meta:
        managed = False
        db_table = 'Sportman'



class Tournaments(models.Model):
    tournamentsid = models.AutoField(db_column='TournamentsId', primary_key=True)  # Field name made lowercase.
    judgename = models.CharField(db_column='JudgeName', max_length=100)  # Field name made lowercase.
    location = models.CharField(db_column='Location', max_length=100)  # Field name made lowercase.
    datetournament = models.DateTimeField(db_column='DateTournament', blank=True, null=True)  # Field name made lowercase.
    finishtournament = MySQLBooleanField(db_column='FinishTournament')  # Field name made lowercase. This field type is a guess.
    nametournament = models.CharField(db_column='NameTournament', max_length=100)  # Field name made lowercase.
    genderman = MySQLBooleanField(db_column='GenderMan')  # Field name made lowercase. This field type is a guess.
    counttours=models.IntegerField(db_column='CountTours')

    def add_count_tours(self):
        count_participant=Particianttournaments.objects.filter(tournaments_tournamentsid=self)
        self.counttours=math.ceil(math.log(count_participant.count(),2))+math.ceil(math.log(3-1,2))
        self.save()

    class Meta:
        managed = False
        db_table = 'Tournaments'



class Tours(models.Model):
    tourid = models.AutoField(db_column='TourId', primary_key=True)  # Field name made lowercase.
    numbertour = models.IntegerField(db_column='NumberTour')  # Field name made lowercase.
    finishtour = MySQLBooleanField(db_column='FinishTour', blank=True)  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Tours'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    start_tournament = MySQLBooleanField()

    def __unicode__(self):
        return self.user

    class Meta:
        verbose_name = 'Профиль'

class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
