# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from tournament.models import *

admin.site.register(Articles)
admin.site.register(Tournaments)
admin.site.register(Sportman)
admin.site.register(Ranksportman)
admin.site.register(Participanttours)
admin.site.register(Particianttournaments)
admin.site.register(Tours)
admin.site.register(Test)

