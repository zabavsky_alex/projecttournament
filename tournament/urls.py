from django.conf.urls import url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from tournament.views import *

urlpatterns = [
    url(r'^$',Home.as_view(),name='home'),
    url(r'^accounts/login/$',Authentication.as_view(),name='authentication'),
    url(r'^accounts/logout/$', UserAuthenticate.as_view(), name='UserAuthenticate'),
    url(r'^accounts/check_in/$',Check_in.as_view(),name='check_in'),
    url(r'^about_tournment/$',AboutTournament.as_view(),name='AboutTournament'),
    url(r'^athlete_card/$',AthleteCard.as_view(),name='AthleteCard'),
    url(r'^athlete_card/add/$',AthleteCard.as_view(),name='athlete_card_add'),
    url(r'^athlete_card/delete/(?P<Sportman_id>[0-9]+)/$',AthleteCard.as_view(), name='athlete_card_delete'),
    url(r'^athlete_card/edit/$',AthleteCard.as_view(), name='athlete_card_edit'),
    url(r'^tournament/new/$',tournament_new, name='tournament_new'),
    url(r'^tournament/new/participant_list/$',tournament_new_list_participant, name='tournament_new_list_participant'),
    url(r'^tournament/new/participant_add/(?P<Sportman_id>[0-9]+)/$',tournamen_new_add_participant, name='tournament_new_add_participant'),
    url(r'^tournament/new/participant_delete/(?P<Participant_id>[0-9]+)/$',tournamen_new_delete_participant, name='tournament_new_delete_participant'),
    url(r'^tournament/new/tour/(?P<Tour_number>[0-9]+)/$',tournament_new_tour, name='tournament_new_tour'),
    url(r'^tournament/result/(?P<Tournament_id>[0-9]+)/$',tournament_result, name='tournament_new_tour_result'),
    url(r'^tournament/archive/$', archive_tournsmenst, name='archive_tournsmenst'),
    url(r'^article/(?P<article_id>[0-9]+)/$', one_article, name='one_article'),
    url(r'^tournament/delete(?P<tournament_id>[0-9]+)/$', delete_tournament, name='delete_tournament'),
]


if settings.DEBUG:
    urlpatterns+=staticfiles_urlpatterns()+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
