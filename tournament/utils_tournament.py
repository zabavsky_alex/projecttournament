from models import *
import random,math

def pair_for_firs_tour(Tournament):
    all_participant=list(Particianttournaments.objects.filter(tournaments_tournamentsid=Tournament))
    all_participant.sort(key=order_by_point_elo_rang_name,reverse=True)

    divider_participant_group=int(len(all_participant))/2
    s1=all_participant[:divider_participant_group]
    s2=all_participant[divider_participant_group:]
    new_tour=Tours.objects.create(numbertour=1,finishtour=False)
    number_group=1
    color_chess_one_pat = True
    color_chess_two_pat = False
    random_color = random.randrange(1, 20)
    print random_color
    if random_color > 10:
        color_chess_one_pat = False
        color_chess_two_pat = True
    while number_group<=divider_participant_group:
        color_chess_one=color_chess_one_pat
        color_chess_two=color_chess_two_pat
        add_participants_tour(s1[number_group-1],number_group,new_tour,color_chess_one,False)
        add_participants_tour(s2[number_group - 1], number_group, new_tour,color_chess_two,False)
        number_group+=1

    if len(all_participant)%2!=0:
         add_participants_tour(all_participant[-1],0,new_tour,False,True)

def order_by_point_elo_rang_name(participant):
    return participant.currentpoint,participant.sportman_sportmanid.ratingelo,participant.sportman_sportmanid.ranksportman_ranksportmanid.ranksportmanid

def add_participants_tour(participant,number_group,new_tour,color_chess,next_tour):
    point_tour=0
    finish_tour=False
    if next_tour== True:
        point_tour=1
        finish_tour=True
        participant_tournament_next_tour=Particianttournaments.objects.get(particianttournamentid=participant.particianttournamentid)
        participant_tournament_next_tour.nexttour=True
        participant_tournament_next_tour.save()
    participant=Participanttours.objects.create(
        groupnumber=number_group,
        finishgroup=finish_tour,
        pointtour=point_tour,
        colorchess=color_chess,
        tours_tourid=new_tour,
        particianttournaments_particianttournamentid=participant)
    participant.save()

class couple_participant_tour:
    def __init__(self,participant_one,participant_two):
        self.participant_one=participant_one
        self.participant_two=participant_two

def couple_add_ratingelo(couple_participant_one,couple_participant_two):
    math_coef_one=couple_participant_one.particianttournaments_particianttournamentid.sportman_sportmanid.currentmatchcoefficient
    math_coef_two = couple_participant_two.particianttournaments_particianttournamentid.sportman_sportmanid.currentmatchcoefficient
    ratingelo_one = couple_participant_one.particianttournaments_particianttournamentid.sportman_sportmanid.ratingelo
    ratingelo_two = couple_participant_two.particianttournaments_particianttournamentid.sportman_sportmanid.ratingelo
    point_tour_one=couple_participant_one.pointtour
    point_tour_two=couple_participant_two.pointtour
    new_elo_one=rating_elo(math_coef_one,ratingelo_one,point_tour_one,ratingelo_two)

    new_elo_two=rating_elo(math_coef_two, ratingelo_two, point_tour_two, ratingelo_one)

    couple_participant_one.elotour = new_elo_one - ratingelo_one
    couple_participant_two.elotour=new_elo_two-ratingelo_two
    couple_participant_one.particianttournaments_particianttournamentid.sportman_sportmanid.ratingelo = new_elo_one
    couple_participant_one.particianttournaments_particianttournamentid.sportman_sportmanid.save()

    couple_participant_two.particianttournaments_particianttournamentid.sportman_sportmanid.ratingelo=new_elo_two
    couple_participant_two.particianttournaments_particianttournamentid.sportman_sportmanid.save()

def rating_elo(math_patient,elo_patient,point_patient,elo_adversary):
    new_elo=float()
    if(int(elo_patient) == 0 and int(elo_adversary) == 0 and float(point_patient) == 1.0):
        new_elo=math_patient/2
    else:
        math_check = 1 / (1 + pow(10,((float(elo_adversary) - float(elo_patient)) / 400)))
        new_elo = elo_patient + (math_patient * (float(point_patient) - math_check))
        if new_elo<0:
            new_elo=0
    return  math.trunc(new_elo)

def pair_for_next_tour(tournament,buf_number_tour):
    all_participant = list(Particianttournaments.objects.filter(tournaments_tournamentsid=tournament))
    all_participant.sort(key=order_by_point_elo_rang_name, reverse=True)
    all_group_point_tournament=list_group_point_tournament(all_participant)
    new_tour = Tours.objects.create(numbertour=buf_number_tour, finishtour=False)
    count_number_point_group=0
    count_number_couple_group=[1]

    print len(all_group_point_tournament)
    for a in all_group_point_tournament:
        print ('----------------------')
        for b in a:
            print b.sportman_sportmanid.sportmanname

    for one_point_group in all_group_point_tournament:
        print ('Participant one_point_group')
        print ('----------------------')
        for a in one_point_group:
             print a.sportman_sportmanid.sportmanname
        print ('----------------------')
        if count_number_point_group+1<len(all_group_point_tournament):
            print('point_group_no_last')
            if len(one_point_group) >= 2:
                remainder_point_group=matching_pairs_in_a_point_group(one_point_group,tournament,buf_number_tour,new_tour,count_number_couple_group)
                print ('--------Posle--------')
                for a in remainder_point_group:
                    print a.sportman_sportmanid.sportmanname
                print ('---------------------')
                if(len(remainder_point_group)>0):
                    print ('Next_point_tour')
                    for a in remainder_point_group:
                        all_group_point_tournament[count_number_point_group + 1].append(a)
            else:
                print ('1 participant')
                all_group_point_tournament[count_number_point_group+1].append(one_point_group[0])
                all_group_point_tournament[count_number_point_group ].remove(one_point_group[0])
        else:
            print ('last point group')
            if len(one_point_group) >= 2:
                print ('participant>2')
                remainder_point_group = matching_pairs_in_a_point_group(one_point_group, tournament, buf_number_tour,
                                                                        new_tour, count_number_couple_group)
                if len(all_participant) % 2 != 0:
                    if (len(remainder_point_group) == 1):
                        if remainder_point_group[0].nexttour==False:
                            add_participants_tour(one_point_group[0], 0, new_tour, False, True)
                        else:
                            print ('Problem1')
                            one_hermits_participan_odd(one_point_group[0], tournament, buf_number_tour,new_tour, count_number_couple_group)
                    elif len(remainder_point_group) > 1:
                        print ('Problem2')
                else:
                    print ('Problem _one_participant_even_all_participant')
                    if (len(remainder_point_group) == 1):
                        print ('Problem3')
                    elif len(remainder_point_group) > 1:
                        print ('Problem4')
                        two_hermits_participan_even(one_point_group, tournament, buf_number_tour, new_tour,
                                                    count_number_couple_group)
                        print ('END4')

            else:
                if len(all_participant)%2!=0:
                    print('1 participant')
                    if one_point_group[0].nexttour==False:
                        add_participants_tour(one_point_group[0], 0, new_tour, False, True)
                    else:
                        print('problem5')
                        one_hermits_participan_odd(one_point_group[0], tournament, buf_number_tour, new_tour)
                else:
                    print ('Problem5 _one_participant_even_all_participant')
                    if (len(remainder_point_group) == 1):
                        print ('Problem6')
                    elif len(remainder_point_group) > 1:
                        print ('Problem 7')
                        two_hermits_participan_even(one_point_group, tournament, buf_number_tour, new_tour,
                                                   count_number_couple_group)
        count_number_point_group+=1

def matching_pairs_in_a_point_group(one_point_group,tournament,buf_number_tour,new_tour,count_number_couple_group):
    one_point_group.sort(key=order_by_point_elo_rang_name, reverse=True)
    divider_participant_group = int(len(one_point_group)) / 2
    s1 = one_point_group[:divider_participant_group]
    s2 = one_point_group[divider_participant_group:]
    for s1_one_participant in s1:
        for s2_one_participant in s2:
            if was_the_game_check(s1_one_participant, s2_one_participant, tournament,
                                  int(buf_number_tour)) == False and couple_already_mathced_check(s1_one_participant,
                                                                                                  buf_number_tour) == False and couple_already_mathced_check(
                    s2_one_participant, buf_number_tour) == False:
                list_color = choose_the_color_of_chess(s1_one_participant, s2_one_participant)
                if buf_number_tour==tournament.counttours:
                    add_participants_tour(s1_one_participant, count_number_couple_group[0], new_tour, list_color[0],
                                          False)
                    add_participants_tour(s2_one_participant, count_number_couple_group[0], new_tour, list_color[1],
                                          False)
                    one_point_group.remove(s1_one_participant)
                    one_point_group.remove(s2_one_participant)
                    count_number_couple_group[0] += 1
                elif color_check(s1_one_participant, s2_one_participant) == True:
                    add_participants_tour(s1_one_participant, count_number_couple_group[0], new_tour, list_color[0], False)
                    add_participants_tour(s2_one_participant, count_number_couple_group[0], new_tour, list_color[1], False)
                    one_point_group.remove(s1_one_participant)
                    one_point_group.remove(s2_one_participant)
                    count_number_couple_group[0] += 1
    return one_point_group

def clear_list(list_patient):
    list_patient=list()
    return list_patient

def list_group_point_tournament(all_participant):
    count_participant=len(all_participant)
    all_group_point_tournament=list(list())
    count=0
    buf_group_point = list()
    for buf_participant in all_participant:
        buf_group_point.append(buf_participant)
        if count+2 <= count_participant:
            if all_participant[count+1].currentpoint != buf_participant.currentpoint:
                all_group_point_tournament.append(buf_group_point)
                buf_group_point=clear_list(buf_group_point)
        else:
            all_group_point_tournament.append(buf_group_point)
        count += 1
    return  all_group_point_tournament

def was_the_game_check(participant_one,participant_two,buf_tournament,buf_tour):
    count_tours_tournament=buf_tournament.counttours
    result=False
    for i in range(1,buf_tour,1):
        participant_tours_one=Participanttours.objects.get(tours_tourid__numbertour=i,
                                                           particianttournaments_particianttournamentid=participant_one)
        participant_tours_two = Participanttours.objects.get(tours_tourid__numbertour=i,
                                                             particianttournaments_particianttournamentid=participant_two)
        if participant_tours_one.groupnumber == participant_tours_two.groupnumber:
            result=True
    return result

def couple_already_mathced_check(participant,buf_tour):
    result=False
    if Participanttours.objects.filter(tours_tourid__numbertour=buf_tour,particianttournaments_particianttournamentid=participant,).exists():
        result=True
    return result

def color_check(participant_one,participant_two):
    result=True
    if ((participant_one.abscolorchess==2 and participant_two.abscolorchess==2)or ((participant_one.abscolorchess==-2 and participant_two.abscolorchess==-2))):
        result=False
    return result

def choose_the_color_of_chess(participant_one,participant_two):
    color_one=True
    color_two=False
    fabs_color_one=math.fabs(participant_one.abscolorchess)
    fabs_color_two=math.fabs(participant_two.abscolorchess)
    if fabs_color_one>=fabs_color_two:
        if participant_one.abscolorchess>0:
            color_one=False
            color_two=True
    else:
        if participant_two.abscolorchess<0:
            color_one=False
            color_two=True
    list_color=list()
    list_color.append(color_one)
    list_color.append(color_two)
    return list_color

def distribution_of_prizes(tournament):
    all_participants_tournament=list(Particianttournaments.objects.filter(tournaments_tournamentsid=tournament))

    for one_participant in all_participants_tournament:
        buchholzcoefficient=0
        solkoffcoefficient=0
        solkoffcoefficienttruncated=0
        min_point_adversary=0
        max_point_adversary = 0
        for i in range(1,tournament.counttours+1,1):
            patient=Participanttours.objects.get(particianttournaments_particianttournamentid=one_participant,tours_tourid__numbertour=i)
            adversary=Participanttours.objects.filter(groupnumber=patient.groupnumber,
                                                    tours_tourid__numbertour=i,
                                                    particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament)
            if len(adversary)>0:
                point_adversary=adversary[0].particianttournaments_particianttournamentid.currentpoint
                buchholzcoefficient+=point_adversary
                if i==1:
                    min_point_adversary=point_adversary
                if min_point_adversary>point_adversary:
                    min_point_adversary=point_adversary
                if max_point_adversary<point_adversary:
                    max_point_adversary=point_adversary
                if i==tournament.counttours:
                    solkoffcoefficient=buchholzcoefficient-min_point_adversary-max_point_adversary
                    solkoffcoefficienttruncated=buchholzcoefficient-min_point_adversary
        one_participant.buchholzcoefficient=buchholzcoefficient
        one_participant.solkoffcoefficient=solkoffcoefficient
        one_participant.solkoffcoefficienttruncated=solkoffcoefficienttruncated
        one_participant.save()
    all_participants_tournament=add_place(all_participants_tournament)

def add_place(all_participant_tournament):
    all_participant_tournament.sort(key=order_by_place, reverse=True)
    count_place=1
    for one_participant in all_participant_tournament:
        one_participant.place=count_place
        count_place+=1
        one_participant.save()
    return all_participant_tournament

def order_by_place(participant):
    return participant.currentpoint,participant.buchholzcoefficient,participant.solkoffcoefficient,participant.solkoffcoefficienttruncated

def two_hermits_participan_even(participant_tournament_none_couple, tournament, buf_number_tour,new_tour, count_number_couple_group_list):
    all_participant = list(Particianttournaments.objects.filter(tournaments_tournamentsid=tournament))
    current_number_couple=len(all_participant)/2
    print ('LEn_all')
    print participant_tournament_none_couple
    flag_participant_none_couple=True
    print ('Y1')
    count_number_couple_group=int(count_number_couple_group_list[0])-1
    for buf_count_group in range(count_number_couple_group,0,-1):
        print ('Range')
        if flag_participant_none_couple == True:
            print ('Y2')
            participant_tour_buf_couple=list(Participanttours.objects.filter(particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament,
                                                                    groupnumber=buf_count_group,
                                                                    tours_tourid=new_tour))
            print ('Y3')
            participant_tour_buf_couple.sort(key=order_participant_tours_by_point_elo_rang_name,reverse=False)
            print ('Y4')
            one_participant_tournament_in_couple=Particianttournaments.objects.get(participanttours=participant_tour_buf_couple[0])
            print ('Y5')
            two_participant_tournament_in_couple = Particianttournaments.objects.get(participanttours=participant_tour_buf_couple[1])
            print ('Y6')
            if was_the_game_check(one_participant_tournament_in_couple,participant_tournament_none_couple[0],tournament,int(buf_number_tour))==False and was_the_game_check(two_participant_tournament_in_couple,participant_tournament_none_couple[1],tournament,int(buf_number_tour))==False:
                print ('Y7')
                participant_tour_delete=Participanttours.objects.filter(particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament,
                                                                    groupnumber=buf_count_group,
                                                                    tours_tourid=new_tour)
                print ('Y8')
                for delete_participant in participant_tour_delete:
                    print ('Y9')
                    delete_participant.delete()
                print ('Y10')
                list_color = choose_the_color_of_chess(one_participant_tournament_in_couple,participant_tournament_none_couple[0])
                print ('Y11')
                add_participants_tour(one_participant_tournament_in_couple, buf_count_group, new_tour, list_color[0],False)
                print ('Y12')
                add_participants_tour(participant_tournament_none_couple[0], buf_count_group, new_tour, list_color[1],False)
                print ('Y13')

                print ('Y14')
                print buf_count_group
                print 'CURRENT NUMBER COUPLE'
                print current_number_couple
                list_color_two = choose_the_color_of_chess(one_participant_tournament_in_couple,
                                                       participant_tournament_none_couple[1])
                print ('Y15')

                add_participants_tour(two_participant_tournament_in_couple, current_number_couple, new_tour, list_color_two[0],
                                      False)
                print ('Y16')
                add_participants_tour(participant_tournament_none_couple[1], current_number_couple, new_tour, list_color_two[1],
                                      False)
                print ('Y17')
                for a in participant_tournament_none_couple:
                    print ('Y18')
                    participant_tournament_none_couple.remove(a)
                print ('Y19')
                flag_participant_none_couple=False

            elif was_the_game_check(two_participant_tournament_in_couple,participant_tournament_none_couple[0],tournament,int(buf_number_tour))==False and was_the_game_check(one_participant_tournament_in_couple,participant_tournament_none_couple[1],tournament,int(buf_number_tour))==False:
                print ('Y20')
                participant_tour_delete = Participanttours.objects.filter(
                      particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament,
                        groupnumber=buf_count_group,
                        tours_tourid=new_tour)
                for delete_participant in participant_tour_delete:
                    delete_participant.delete()
                list_color = choose_the_color_of_chess(one_participant_tournament_in_couple,
                                                       participant_tournament_none_couple[1])
                add_participants_tour(one_participant_tournament_in_couple, buf_count_group, new_tour, list_color[0],
                                      False)
                add_participants_tour(participant_tournament_none_couple[1], buf_count_group, new_tour, list_color[1],
                                      False)


                list_color_two = choose_the_color_of_chess(one_participant_tournament_in_couple,
                                                       participant_tournament_none_couple[0])
                add_participants_tour(two_participant_tournament_in_couple, current_number_couple, new_tour,
                                      list_color_two[0],
                                      False)
                add_participants_tour(participant_tournament_none_couple[0], current_number_couple, new_tour,
                                      list_color_two[1],
                                      False)
                for a in participant_tournament_none_couple:
                    participant_tournament_none_couple.remove(a)
                flag_participant_none_couple = False
            print ('-----------------------------------------')
        else:
            print ('break')
            break
        print ('-------------ITER--------------------------')


def one_hermits_participan_odd(participant_tournament_none_couple, tournament, buf_number_tour,new_tour, count_number_couple_group_list):
    flag_participant_none_couple=True
    count_number_couple_group=int(count_number_couple_group_list[0])-1
    for buf_count_group in range(count_number_couple_group,0,-1):
        if flag_participant_none_couple == True:
            participant_tour_buf_couple=list(Participanttours.objects.filter(particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament,
                                                                    groupnumber=buf_count_group,
                                                                    tours_tourid=new_tour))
            participant_tour_buf_couple.sort(key=order_participant_tours_by_point_elo_rang_name,reverse=False)
            one_participant_tournament_in_couple=Particianttournaments.objects.get(participanttours=participant_tour_buf_couple[0])
            two_participant_tournament_in_couple = Particianttournaments.objects.get(participanttours=participant_tour_buf_couple[1])
            if was_the_game_check(one_participant_tournament_in_couple,participant_tournament_none_couple,tournament,int(buf_number_tour))==False and two_participant_tournament_in_couple.nexttour==False:
                   participant_tour_delete=Participanttours.objects.filter(particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament,
                                                                    groupnumber=buf_count_group,
                                                                    tours_tourid=new_tour)
                   for delete_participant in participant_tour_delete:
                       delete_participant.delete()
                   list_color = choose_the_color_of_chess(one_participant_tournament_in_couple,participant_tournament_none_couple)
                   add_participants_tour(one_participant_tournament_in_couple, buf_count_group, new_tour, list_color[0],False)
                   add_participants_tour(participant_tournament_none_couple, buf_count_group, new_tour, list_color[1],False)
                   add_participants_tour(two_participant_tournament_in_couple, 0, new_tour, False,True)
                   flag_participant_none_couple=False

            elif was_the_game_check(two_participant_tournament_in_couple,participant_tournament_none_couple,tournament,int(buf_number_tour))==False and one_participant_tournament_in_couple.nexttour==False:
                   participant_tour_delete = Participanttours.objects.filter(
                        particianttournaments_particianttournamentid__tournaments_tournamentsid=tournament,
                        groupnumber=buf_count_group,
                        tours_tourid=new_tour)
                   for delete_participant in participant_tour_delete:
                        delete_participant.delete()
                   list_color = choose_the_color_of_chess(two_participant_tournament_in_couple,participant_tournament_none_couple)
                   add_participants_tour(two_participant_tournament_in_couple, buf_count_group, new_tour, list_color[0],
                                         False)
                   add_participants_tour(participant_tournament_none_couple, buf_count_group, new_tour, list_color[1],
                                         False)
                   add_participants_tour(one_participant_tournament_in_couple, 0, new_tour, False, True)
                   flag_participant_none_couple=False
        else:
            break

def order_participant_tours_by_point_elo_rang_name(participant):
        return participant.particianttournaments_particianttournamentid.currentpoint, participant.particianttournaments_particianttournamentid.sportman_sportmanid.ratingelo, participant.particianttournaments_particianttournamentid.sportman_sportmanid.ranksportman_ranksportmanid.ranksportmanid
